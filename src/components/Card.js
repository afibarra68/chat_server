import React, { Component } from "react";
import "./styles/card.css";
import phone from "../assets/portrait/phone-11.png";

class Card extends Component {
  render() {
    return (
      <div className="Card-Container">
        <div className="card-sector">
          <div className="card">
            <div className="Card-first">
              <div className="Card-body">
                <div className="container-body-1">
                  <p className="Card-Text">Feel the true power</p>
                </div>
                <div className="container-body-2">
                  <img className="image-card" alt="any" src={phone} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Card;
