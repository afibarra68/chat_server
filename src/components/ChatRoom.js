import React, { Component } from "react";

import Card from "./Card";

class ChatRoom extends Component {
  constructor() {
    super();
    this.state = {
      message: "",
      messages: [],
    };
  }

  handleSubmit(e) {
    e.preventDefault();
    const list = this.state.messages;
    const newMessage = {
      id: this.state.messages.length,
      text: this.state.message,
    };
    list.push(newMessage);
    this.setState({ messages: list });
    this.setState({ message: "" });
  }

  updateMessage(e) {
    this.setState({ message: e.target.value });
  }

  render() {
    const { messages } = this.state;
    const messagesList = messages.map((message) => {
      return <li key="messages.id">{message.text}</li>;
    });
    return (
      <div>
        <Card />,
        <ul> {messagesList} </ul>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <input
            className="input-data.chat"
            type="text"
            onChange={this.updateMessage.bind(this)}
            value={this.state.message.value}
          />
          <button>send</button>
        </form>
      </div>
    );
  }
}

export default ChatRoom;
